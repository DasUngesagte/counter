#include <curses.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// https://gist.github.com/yuanqing/ffa2244bd134f911d365
#define HEIGHT 5

#define ESC "\e"
#define CSI "["
#define ED "1J"
#define CPL "F"

char* ascii_art_numbers[10][HEIGHT] = {
   {" 0000 ",
    "00  00",
    "00  00",
    "00  00",
    " 0000 "},

   {"1111  ",
    "  11  ",
    "  11  ",
    "  11  ",
    "111111"},

   {" 2222 ",
    "22  22",
    "   22 ",
    "  22  ",
    "222222"},

   {" 3333 ",
    "33  33",
    "   333",
    "33  33",
    " 3333 "},

   {"44  44",
    "44  44",
    "444444",
    "    44",
    "    44"},

   {"555555",
    "55    ",
    "55555 ",
    "    55",
    "55555 "},

   {" 6666 ",
    "66    ",
    "66666 ",
    "66  66",
    " 6666 "},

   {"777777",
    "   77 ",
    "  77  ",
    " 77   ",
    "77    "},

   {" 8888 ",
    "88  88",
    " 8888 ",
    "88  88",
    " 8888 "},

   {" 9999 ",
    "99  99",
    " 99999",
    "    99",
    " 9999 "}
};

void print_number(unsigned number){

    // Convert our number to a string for easier digit access:
    char number_str[21] = {};                                       // Enough digits to store a size_t
    sprintf(number_str, "%d", number);

    size_t num_digits = strlen(number_str);

    for(size_t cur_height = 0; cur_height < HEIGHT; ++cur_height){
        for(size_t digit = 0; digit < num_digits; ++digit){
            int current_num = number_str[digit] - '0';              // Get the current digit
            char** current_str = ascii_art_numbers[current_num];    // Get the corresponding string
            printf(" %s ", current_str[cur_height]);         // Print the current part
        }
        printf("\n\r");
    }
}

int main(int argc, char* argv[]){

    // curses stuff:
    initscr();
    cbreak();
    noecho();
    refresh();

    size_t counter = argc > 1 ? strtol(argv[1], NULL, 0) : 0;

    printf("\n");
    print_number(counter);
    printf(ESC CSI "%d" CPL, HEIGHT);

    int running = 1;
    int chr;
    while(running) {
        chr = getch();
        switch(chr){
            case EOF:   // Fallthrough, same as '\e'
            case '\e': running = 0; break;
            case '-': counter = counter > 0 ? counter - 1 : 0; break;
            default: counter++;
        }

        print_number(counter);

        // Clear screen for next print:
        printf(ESC CSI ED);
        printf(ESC CSI "%d" CPL, HEIGHT);
    }
    endwin();
}
